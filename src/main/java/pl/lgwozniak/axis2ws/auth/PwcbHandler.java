package pl.lgwozniak.axis2ws.auth;

import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.ws.security.WSPasswordCallback;

/**
 *
 * @author lwozniak
 */
public class PwcbHandler implements CallbackHandler {

    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for (Callback cb : callbacks) {
            if (cb instanceof WSPasswordCallback) {
                doAuthorization((WSPasswordCallback) cb);
            } else {
                throw new UnsupportedCallbackException(cb, "nieprawidłowy callback");
            }
        }
    }

    private void doAuthorization(WSPasswordCallback pc) throws IOException {
        try {                       
            if("tk".equals(pc.getIdentifier())){
                pc.setPassword("pass");                
                return;
            }                   
        } catch (Exception e) {
            throw new IOException(e.toString());
        }
    }

}
