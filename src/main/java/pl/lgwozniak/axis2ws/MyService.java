package pl.lgwozniak.axis2ws;

import java.util.Calendar;

/**
 *
 * @author lwozniak
 */
public class MyService {
    public String sayHello(String name){      
        return "Witaj " + name + "," +Calendar.getInstance().getTime();
    }
}
