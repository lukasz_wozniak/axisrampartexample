package pl.lgwozniak.axis2ws.web;

import java.rmi.RemoteException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import pl.lgwozniak.axis2ws.client.MyServiceClient;

/**
 *
 * @author lwozniak
 */
@ManagedBean()
@SessionScoped
public class HelloWorldBean {

    private String msg ;
    private String result;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
   
    
    public void submit() throws RemoteException{
        MyServiceClient client = new MyServiceClient();
        result = client.sayHello(msg);
        System.out.println("odpowiedz " + result);
    }    
}
