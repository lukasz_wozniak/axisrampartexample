package pl.lgwozniak.axis2ws.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author lwozniak
 */
@Path("developers")
@Consumes("application/json")
public class Developers {
    
    @GET
    public String developer(@Context final HttpServletResponse response) throws IOException{
        response.setStatus(Response.Status.ACCEPTED.getStatusCode());
        response.flushBuffer();
        return "Lukas";
    }
    
    @GET
    @Path("{first}-{last}")
    public List<Developer> developer(@PathParam("first") String first, @PathParam("last") String last){
        List r = new ArrayList();
        r.add(new Developer(first,last));
        return r;
    }
}
