package pl.lgwozniak.axis2ws.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lwozniak
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Developer {
    
    private String fistName;
    private String lastName;

    public Developer(String fistName, String lastName) {
        this.fistName = fistName;
        this.lastName = lastName;
    }

    public Developer() {
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    

}
