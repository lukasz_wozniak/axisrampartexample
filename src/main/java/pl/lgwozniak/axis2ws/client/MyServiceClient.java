package pl.lgwozniak.axis2ws.client;

import java.rmi.RemoteException;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import pl.lgwozniak.ws.client.MyService;
import pl.lgwozniak.ws.client.MyServiceStub;

/**
 *
 * @author lwozniak
 */
public class MyServiceClient  implements MyService{

    MyServiceStub stub = null;
    public MyServiceClient() {
        try{
            
            ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem("E:/axis2", null);
            stub = new MyServiceStub(ctx,"http://localhost:8084/Axis2WS/services/MyService");
            new SecurityHandler(stub._getServiceClient()).setUpRampart();
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    
    @Override
    public String sayHello(String name) throws RemoteException {
        return stub.sayHello(name);
    }

}
