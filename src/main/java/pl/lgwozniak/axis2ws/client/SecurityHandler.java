package pl.lgwozniak.axis2ws.client;

import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.neethi.PolicyEngine;
import org.apache.rampart.RampartMessageData;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.rampart.policy.model.RampartConfig;
import org.apache.ws.security.handler.WSHandlerConstants;
import pl.lgwozniak.axis2ws.auth.PwcbHandler;

/**
 *
 * @author lwozniak
 */
public class SecurityHandler {
    
    private ServiceClient client;

    public SecurityHandler(ServiceClient client) {
        this.client = client;
    }
    
    public void setUpRampart() throws Exception{
        try{
            OutflowConfiguration oc = new OutflowConfiguration();
            oc.setActionItems("UsernameToken");
            oc.setUser("tk");
            oc.setPasswordCallbackClass("pl.lgwozniak.axis2ws.auth.PwcbHandler");
            oc.setPasswordType("PasswordText");
            Options options = client.getOptions();
            options.setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, oc.getProperty());
            client.engageModule("rampart");
        }catch(Exception e){
            throw e;
        }
    }

}
